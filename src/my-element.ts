import { LitElement, html, customElement } from '@polymer/lit-element'

@customElement("my-element" as any)
export class MyElement extends LitElement {
	protected render() {
		return html`
			<h1>my-element</h1>
		`;
	}
}