# lit-starter-kit

This is a bare repository used for creating new lit-element components.

## Getting Started

```bash
# First create the repository
git init
# Then pull the template
git pull https://bitbucket.org/rammbulanz/lit-component-template master
# Install the node_modules
yarn install
```

Then go ahead and replace the names (src/my-element.ts, package.json)

Done. You're able to start

Don't forget to put some demos.